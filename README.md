<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](http://doctoc.herokuapp.com/)*

- [Introduction to Protractor and Aurelia](#introduction-to-protractor-and-aurelia)
  - [Getting Started](#getting-started)
    - [What is Protractor?](#what-is-protractor)
      - [Wait for Angular](#wait-for-angular)
      - [Get elements by binding](#get-elements-by-binding)
      - [Get elements by repeaters](#get-elements-by-repeaters)
    - [Why use Protractor ?](#why-use-protractor-)
    - [Installing Protractor](#installing-protractor)
  - [Writing our First Test](#writing-our-first-test)
    - [Protractor configuration](#protractor-configuration)
    - [Simple Example](#simple-example)
    - [Running E2E Tests](#running-e2e-tests)
    - [Protractor Syntax](#protractor-syntax)
    - [Where Protractor First in the Testing Spectrum](#where-protractor-first-in-the-testing-spectrum)
  - [Locators](#locators)
    - [valueBind](#valuebind)
    - [srcBind](#srcbind)
    - [clickTrigger](#clicktrigger)
    - [showBind](#showbind)
    - [optionBind](#optionbind)
    - [by.css](#bycss)
  - [Test Suites](#test-suites)
  - [Using Protractor as Part of a Development Workflow](#using-protractor-as-part-of-a-development-workflow)
  - [Page Objects](#page-objects)
    - [Common Problems with UI Testing](#common-problems-with-ui-testing)
      - [UI Changes](#ui-changes)
      - [Lots of Repetition](#lots-of-repetition)
      - [Broken Tests](#broken-tests)
    - [Option 1: Manually Updating References](#option-1-manually-updating-references)
    - [Option 2: Page Objects](#option-2-page-objects)
      - [Maintainable Tests](#maintainable-tests)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Introduction to Protractor and Aurelia

> Notes from [Protractor](http://www.protractortest.org/#/) course on [Pluralsight](https://app.pluralsight.com/library/courses/protractor-introduction/table-of-contents), updated to work with [Aurelia](http://aurelia.io/).

## Getting Started

### What is Protractor?

Official definition: "Protractor is an end-to-end test framework for AngularJS applications".

To understand end-to-end testing, compare to unit testing:

| End-to-End                                 | Unit Testing                                    |
| ------------------------------------------ |------------------------------------------------ |
| Interact with the entire application stack | Interact with a single, isolated unit at a time |
| Focused on what the user experiences       | Focused on the functionality of the code        |

Protractor is able to interact with the Angular application (as opposed to just the web site), because it knows how Angualr works. This makes writing e2e test easier. Some features include:

#### Wait for Angular

When a new view is navigated to in Angular, and that view makes an http request to fetch data, Protractor can monitor that request, and know that Angular will not be ready to display the view until that request returns a response. Therefore writing the test is simpler, no need to add custom logic to wait for page to be ready.

#### Get elements by binding

Protractor can get information from the screen using data bindings. For example, if there is a `person` object in template, and it displays somewhere on the screen the person's name using `{{ person.name }}`. But looking at the rendered DOM, it will only show `<div class="whatever">... some dom structure etc... Jon Smith ... </div>`. Protractor knows that its a binding, so the test can retrieve an element whose value is "person.name".

#### Get elements by repeaters

Protractor is aware of `ng-repeat` and can get sections of the page that are within an `ng-repeat` iterator.

### Why use Protractor ?

__To test like a user, need to _think_ like a user:__

* Actions are rarely standalone (eg: user does not just login in to a website, then do nothing else)
* A user only has access to what is displayed (eg: if user makes some changes to their shopping cart, and it is saved successfully in database, but screen is not refreshed, from a _user_ perspective, the save did not work)
* Systems have many different _personas_ (eg: sales person, sales manager, CFO, IT Admin, each has different things they're allowed to do and see within the same application)

__To improve test efficiency:__

Consider a "normal" development & test flow timeline:

1. Develop feature on a branch
1. Test feature on a branch ***
1. Integrate feature, pull latest mainline into branch
1. Test previous features to ensure mainline features don't break new feature and that new feature doesn't break mainline features ***
1. Release
1. Repeat

*** At these points in the timeline, have to get into the "Cycle of Tests":

* Have to make sure it works (aka Happy Path)
* Make sure it doesn't break with bad or invalid data
* There might be side effects with another feature
* Older features that have been in the system since day one (eg: create a new user), get _benefit of the doubt_, they don't get tested as rigorously as newer features
* As application grows in size, number of features and interactions to test becomes so huge, it becomes a _burden_ to test

Manually testing the application is painful, extremely repetitive and time consuming, time that could be spent developing new features.

__Enter Protractor__

* Automation can replace manual repetitive testing, just give it a suite of tests to run and it will run them over and over.
* Always remembers to check everything
* Runs while you're working, can interact with the browser faster than a human, frees up developers to do development rather than manual testing

### Installing Protractor

First install nodejs. Then `npm install -g protractor`, or install it specifically in a project.

Protractor is built upon selenium, therefore also have to run `webdriver-manager update`

Then run `webdriver-manager start` to kick off the server. Console shows server address such as `http://127.0.0.1:4444/wd/hub`

## Writing our First Test

### Protractor configuration

[Example](sample-project/protractor.conf.js)

Optional `onPrepare` function to position test browser window, could have a user log in here, etc.

### Simple Example

[Example](sample-project/test/e2e/src/example.spec.js)

Tests are written in Jasmine.

`browser` is an object exposed by Protractor. Can use it to navigate to a page:

```javascript
browser.loadAndWaitForAureliaPage('http://localhost:9000');
```

Protractor will wait for the page to finish loading before executing any other test steps, when a `browser.loadAndWaitForAureliaPage` is issued.

`element` is another object exposed by Protractor.

`element.getText()` returns a promise that will be resolved with the element's visible text. Protractor patches Jasmine expect such that `expect` will wait for the promise to resolve before evaluating the assertion, so this works:

```javascript
let someEl = element(by.css('.some-class'));
expect(someEl.getText()).toEqual('expected inner text');
```

`element.click()` issues a click in the browser on that element.

If a click navigates to another view, call `browser.waitForRouterComplete()` to have the browser wait for navigation to finish before doing any other test steps.

`browser.getCurrentUrl()` returns a promise that resolves with the current url. Can use this together with jasmine `expect(...).toEqual('something...')`.

### Running E2E Tests

Using Aurelia Skeleton ES2016, first time, update/install webdriver, then run the app:

```shell
gulp webdriver-update
gulp watch
```

Then in another terminal window, run the tests

```
gulp e2e
```

Protractor starts by launching Chrome, then loading url provided in argument to `browser.loadAndWaitForAureliaPage(url)`. When the tests have finished, Protractor shuts down Chrome.

### Protractor Syntax

Overall syntax is much the same as writing unit tests with Jasmine. `describe`, `beforeEach`, `it`, `expect` are all there. Big difference is `browser` and `element` which are objects exposed by Protractor.

### Where Protractor First in the Testing Spectrum

Consider types of tests: Unit -> Integration -> User Acceptance.

Protractor fits somewhere beteen Integration and User Acceptance testing.

__Similarities with Integration Tests__

* Nothing is mocked out (no mocks, spies etc are used), it runs through the full stack of the application including database.
* Not limited to a particular unit

__Differences from Integration Tests__

* No internal awareness of the application code, for example, cannot access database directly.

__Similarities with User Acceptance Tests__

* Interacts with the UI (i.e. browser)
* Simulates a user navigating, clicking, entering data, looking at data on the screen, etc.
* Ability to test a workflow

__Differences from User Acceptance Tests__

* Humans do not enter the exact same data each time

## Locators

Locators are functions that help Protractor find and return a specific element from the DOM. Locators are native to Selenium, therefore accessible via Protractor which runs on top of Selenium. In addition to the native Locators, Protractor also provides some additional protractor-specific locators.

All locators start with `by.`. Aurelia-specific locators are provided via [this plugin](sample-project/aurelia.protractor.js).

### valueBind

`by.valueBind` finds an input element by Aurelia value binding. For example:

```html
<input type="text" value.bind="firstName" class="form-control" id="fn" placeholder="first name">
```

```javascript
element(by.valueBind('firstName'))
```

### srcBind

`by.srcBind` finds an element by src binding. For example:

```html
<img src.bind="user.avatar_url" crossorigin ref="image"/>
```

```javascript
element(by.srcBind('user.avatar_url'));
```

### clickTrigger

`by.clickTrigger` finds element by value specified in click.trigger. For example:

```html
<button click.trigger="doSomething()" class="btn btn-default">Do Something</button>
```

```javascript
// Click the button
element(by.clickTrigger('doSomething()')).click();
```

### showBind

`by.showBind` finds element by show binding, For example:

```html
<div show.bind="showSomething" class="show-something">Woo Hoo</div>
```

```javascript
let showSomthingElement = element(by.showBind('showSomething'));

// Verify showSomething is not visible
expect(showSomthingElement.isDisplayed()).toBe(fal se);

// Do something that makes it visible, example click a button
element(by.clickTrigger('doSomething()')).click();

// Verify showSomething is now visible
expect(showSomthingElement.isDisplayed()).toBe(true);
```

### optionBind

`by.optionBind` finds option elements inside of a select by value binding. For example:

```html
<select value.bind="favPet" class="form-control">
  <option>Select A Pet</option>
  <option repeat.for="pet of pets" value.bind="pet">${pet}</option>
</select>
```

```javascript
// assume one of favorite pet options is "turtle", then select it
element.all(by.optionBind('pet')).then((petOptions) => {
  petOptions.forEach((opt) => {
    opt.getText().then(optText => {
      if (optText === 'turtle') {
        opt.click();
      }
    });
  });
});
```

### by.css

`by.css` (not Aurelia specific) finds an element by a css selector. This can be any css selector used in a stylesheet or jQuery. For example, given a template:

```html
<div class="primary header">Something Important</div>
```

The header element can be found via:

```javascript
element(by.css('.primary.header'));
```

See the [Protractor Docs](http://www.protractortest.org/#/api?view=ProtractorBy) for all built-in locators.

## Test Suites

Not all tests are created equal. For example:

* Areas Tested - focused on different areas of the application such as login, search, checkout, etc.

* Types of Test - for example smoke test to just make sure everything basically works.

* Length of Tests - for exmaple smoke tests will be quick but other tests may be longer due to more detailed workflow.

Its not ideal to have all these different tests in one runner, better to split them up. Protractor supports this via _Test Suites_.  To enable them, add `suites` config to [protractor.conf.js ](sample-project/protractor.conf.js).

```javascript
suites: {
  suite1: 'path.to.tests',
  suite2: 'path.to.different.tests',
  suite3: 'more.tests',
  full: 'path.to.all.tests'
}
```

To run just a particular suite, for example `suite3`:

```shell
gulp e2e --suite=suite3
```

To run multiple suites, for example `suite1` and `suite3`:

```shell
gulp e2e --suite=suite1,suite3
```

Note this required some changes to [gulp e2e task](sample-project/build/tasks/e2e.js) to support passing command line arguments to protractor.

## Using Protractor as Part of a Development Workflow

Typical workflow without Protractor:

![SDLC Without Protractor](images/sdlc-no-protractor.png "SDLC Without Progractor")

Now add in Protractor:

![SDLC With Protractor](images/sdlc-with-protractor.png "SDLC With Progractor")

## Page Objects

### Common Problems with UI Testing

#### UI Changes

For example, button text or location may change. For some UI testing frameworks, if looking for elements in a particular location in the DOM, this will break the test.  With Protractor, this is solved by using a framework-sepcific locator, such as `click.trigger` to locate a button by the action it performs.

#### Lots of Repetition

For example, might have a lot of tests that have to fill out the same form to do a workflow. So if the form changes, will have multiple places in tests that need to change:

```javascript
element(by.valueBind('user.firstName')).clear().sendKeys('John');
element(by.valueBind('user.lastName')).clear().sendKeys('Smith');
element(by.valueBind('user.occupation')).clear().sendKeys('Software Developer');
```

There will be some refactoring of tests as the code changes, but want to minimize how much time is spent in test maintenance.

#### Broken Tests

Particularly with UI testing, its possible to have a large number of tests break (eg: 25 failures out of 30 tests), not because there are actually 25 different things broken in the application, but because something common they all use has changed, for example a model change from `user.firstName` to `user.first`. This causes developers to lose confidence in the test suite.

### Option 1: Manually Updating References

For example, if change `<input value.bind="user.userName">` to `<input value.bind="user.name"` in template, manually find and replace all occurrences in e2e tests from `element(by.valueBind('user.userName'))` to `element(by.valueBind('user.name'))`.

This can work for a very small number of tests, but doesn't scale to large number of tests and multiple test suites, as the project grows.

### Option 2: Page Objects

[Example PO](sample-project/test/e2e/src/example.po.js) and [Usage in Test](sample-project/test/e2e/src/example2.spec.js)

From [Protractor Docs](https://github.com/angular/protractor/blob/master/docs/page-objects.md): When writing end-to-end tests, a common pattern is to use Page Objects. Page Objects help you write cleaner tests by encapsulating information about the elements on your application page. A Page Object can be reused across multiple tests, and if the template of your application changes, you only need to update the Page Object.

Example:

```javascript
// welcome.po.js
export class PageObjectWelcome {
  constructor() { }

  setFirstName(value) {
    return element(by.valueBind('firstName')).clear().sendKeys(value);
  }
}
```

Usage in test:

```javascript
import {PageObjectWelcome} from './welcome.po.js';

describe('Welcome Page', () => {
  let poWelcome;

  beforeEach(() => {
    poWelcome = new PageObjectWelcome();
  });

  it('Displays full name', () => {
    poWelcome.setFirstName('Joe');
    // ...
  });
});
```

Now if `firstName` binding changes, for example to `first`, only have to make the change once in `PageObjectWelcome`. All tests that use it will automatically now be using the new binding name.

#### Maintainable Tests

Using Page Objects makes tests more maintainable and easy to read.
