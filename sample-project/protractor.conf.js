/*eslint-disable */
// An example configuration file.
exports.config = {
  // communicate directly Chrome Driver or Firefox Driver, bypassing Selenium Server.
  // https://github.com/angular/protractor/blob/master/docs/server-setup.md#connecting-directly-to-browser-drivers
  directConnect: true,

  onPrepare: function() {
    var SpecReporter = require('jasmine-spec-reporter');
    jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: 'all'}));

    browser.driver.manage().window().setPosition(1920, 0);
  },

  // Capabilities to be passed to the webdriver instance.
  capabilities: {
    'browserName': 'chrome'
  },

  // ignored because directConnect is set to true
  //seleniumAddress: 'http://0.0.0.0:4444',

  specs: ['test/e2e/dist/**/*.js'],

  suites: {
    welcomeArea: 'test/e2e/dist/suites/areas/welcome-area.spec.js',
    userArea: 'test/e2e/dist/suites/areas/user-area.spec.js',
    full: 'test/e2e/dist/**/*.js'
  },

  plugins: [{
    path: 'aurelia.protractor.js'
  }],


  // Options to be passed to Jasmine-node.
  jasmineNodeOpts: {
    print: function() {},
    showColors: true,
    defaultTimeoutInterval: 30000
  }
};
