'use strict';

var _welcomePo = require('./welcome.po.js');

var _skeletonPo = require('./skeleton.po.js');

describe('aurelia skeleton app', function () {
  var poWelcome = void 0;
  var poSkeleton = void 0;

  beforeEach(function () {
    poSkeleton = new _skeletonPo.PageObjectSkeleton();
    poWelcome = new _welcomePo.PageObjectWelcome();

    browser.loadAndWaitForAureliaPage('http://localhost:9000');
  });

  it('should load the page and display the initial page title', function () {
    expect(poSkeleton.getCurrentPageTitle()).toBe('Welcome | Aurelia');
  });

  it('should display greeting', function () {
    expect(poWelcome.getGreeting()).toBe('Welcome to the Aurelia Navigation App!');
  });

  it('should automatically write down the fullname', function () {
    poWelcome.setFirstname('Rob');
    poWelcome.setLastname('Eisenberg');

    browser.sleep(200);
    expect(poWelcome.getFullname()).toBe('ROB EISENBERG');
  });

  it('should show alert message when clicking submit button', function () {
    expect(poWelcome.openAlertDialog()).toBe(true);
  });

  it('should navigate to users page', function () {
    poSkeleton.navigateTo('#/users');
    expect(poSkeleton.getCurrentPageTitle()).toBe('Github Users | Aurelia');
  });
});