import {Welcome} from '../../src/welcome';

describe('Welcome', () => {
  let welcome;

  beforeEach(() => {
    welcome = new Welcome();
  });

  describe('fullName', () => {
    it('returns default value', () => {
      expect(welcome.fullName).toEqual('John Doe');
    });
  });
});
