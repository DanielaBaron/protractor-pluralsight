import {DateFormatValueConverter} from '../../src/date-format';

class AppConfigStub {
  getDateFormat() {
    return 'MMMM Do YYYY';
  }
}

describe('Date Format', () => {
  let dateFormat;
  let mockAppConfig;

  beforeEach(() => {
    mockAppConfig = new AppConfigStub();
    dateFormat = new DateFormatValueConverter(mockAppConfig);
  });

  it('Formats date using configured format', () => {
    spyOn(mockAppConfig, 'getDateFormat').and.callThrough();
    let value = '2016-04-30';
    expect(dateFormat.toView(value)).toEqual('April 30th 2016');
    expect(mockAppConfig.getDateFormat.calls.count()).toEqual(1);
  });
});
