/*eslint no-trailing-spaces: 0*/
describe('Welcome Area', () => {
  let appRoot = 'http://localhost:9000';
  beforeEach(() => {
    browser.loadAndWaitForAureliaPage(appRoot);
  });

  it('Shows something when button is clicked', () => {
    // No need to navigate to Welcome view because that's the default
    let showSomthingElement = element(by.showBind('showSomething'));

    // Verify showSomething is not visible
    expect(showSomthingElement.isDisplayed()).toBe(false);

    // Click the button
    element(by.clickTrigger('doSomething()')).click();

    // For now there is a timing issue with the binding.
    // Until resolved we will use a short sleep to overcome the issue.
    browser.sleep(200);

    // Verify showSomething is now visible
    expect(showSomthingElement.isDisplayed()).toBe(true);
  });

  it('Displays form information', () => {
    // Fill in the welcome form
    element(by.valueBind('firstName')).clear().sendKeys('Alice');
    element(by.valueBind('lastName')).clear().sendKeys('Wonders');
    element.all(by.optionBind('pet')).then((petOptions) => {
      petOptions.forEach((opt) => {
        opt.getText().then(optText => {
          if (optText === 'turtle') {
            opt.click();
          }
        });
      });
    });

    // For now there is a timing issue with the binding.
    // Until resolved we will use a short sleep to overcome the issue.
    browser.sleep(200);

    // Verify the message
    expect(element(by.css('.e2e-form-message')).getText()).toEqual('ALICE WONDERS has a TURTLE');
  });
});
