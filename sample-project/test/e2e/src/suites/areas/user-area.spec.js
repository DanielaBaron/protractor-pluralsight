/*eslint no-trailing-spaces: 0*/
describe('User Area', () => {
  let appRoot = 'http://localhost:9000';
  beforeEach(() => {
    browser.loadAndWaitForAureliaPage(appRoot);
  });

  it('Lists users', () => {
    // Navigate to users view (how does this work, there is no css like this???)
    element(by.css('a[href="' + '#/users' + '"]')).click();

    // Provided by Aurelia protractor plugin to wait until navigation is complete
    browser.waitForRouterComplete();
    expect(browser.getTitle()).toBe('Github Users | Aurelia');

    // Verify each user has a name
    element.all(by.css('.e2e-user-name')).then((userElements) => {
      userElements.forEach((userElement) => {
        expect(userElement.getText()).not.toBe(null);
      });
    });

    // Verify each user has an avatar
    element.all(by.srcBind('user.avatar_url')).then((avatarElements) => {
      avatarElements.forEach((avatarElement) => {
        expect(avatarElement.getAttribute('src')).toMatch(/^https\:/);
      });
    });
  });
});
