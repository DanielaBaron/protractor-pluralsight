/*eslint no-trailing-spaces: 0*/
describe('Example', () => {
  let appRoot = 'http://localhost:9000';
  beforeEach(() => {
    browser.loadAndWaitForAureliaPage(appRoot);
  });

  it('Lists users', () => {
    // Navigate to users view (how does this work, there is no css like this???)
    element(by.css('a[href="' + '#/users' + '"]')).click();

    // Provided by Aurelia protractor plugin to wait until navigation is complete
    browser.waitForRouterComplete();
    expect(browser.getTitle()).toBe('Github Users | Aurelia');

    // Verify each user has a name
    element.all(by.css('.e2e-user-name')).then((userElements) => {
      userElements.forEach((userElement) => {
        expect(userElement.getText()).not.toBe(null);
      });
    });

    // Verify each user has an avatar
    element.all(by.srcBind('user.avatar_url')).then((avatarElements) => {
      avatarElements.forEach((avatarElement) => {
        expect(avatarElement.getAttribute('src')).toMatch(/^https\:/);
      });
    });
  });

  it('Child router', () => {
    // Navigate to Child router view
    element(by.css('a[href="' + '#/child-router' + '"]')).click();
    browser.waitForRouterComplete();
    expect(browser.getTitle()).toBe('Welcome | Child Router | Aurelia');

    // This fails intermittently, needs investigation
    // Navigate to users from child routes
    // element(by.href('#/child-router/users')).click();
    // // browser.waitForRouterComplete();
    // browser.sleep(500); // flaky
    // expect(browser.getTitle()).toBe('Github Users | Child Router | Aurelia');
    //
    // // Verify url
    // expect(browser.getCurrentUrl()).toEqual(`${appRoot}/#/child-router/users`);
  });

  it('Shows something when button is clicked', () => {
    // No need to navigate to Welcome view because that's the default
    let showSomthingElement = element(by.showBind('showSomething'));

    // Verify showSomething is not visible
    expect(showSomthingElement.isDisplayed()).toBe(false);

    // Click the button
    element(by.clickTrigger('doSomething()')).click();

    // For now there is a timing issue with the binding.
    // Until resolved we will use a short sleep to overcome the issue.
    browser.sleep(200);

    // Verify showSomething is now visible
    expect(showSomthingElement.isDisplayed()).toBe(true);
  });

  it('Displays form information', () => {
    // Fill in the welcome form
    element(by.valueBind('firstName')).clear().sendKeys('Alice');
    element(by.valueBind('lastName')).clear().sendKeys('Wonders');
    element.all(by.optionBind('pet')).then((petOptions) => {
      petOptions.forEach((opt) => {
        opt.getText().then(optText => {
          if (optText === 'turtle') {
            opt.click();
          }
        });
      });
    });

    // For now there is a timing issue with the binding.
    // Until resolved we will use a short sleep to overcome the issue.
    browser.sleep(200);

    // Verify the message
    expect(element(by.css('.e2e-form-message')).getText()).toEqual('ALICE WONDERS has a TURTLE');
  });
});
