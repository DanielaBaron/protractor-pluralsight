/**
 * Rewrite example.spec.js with Page Objects
 */
 /*eslint no-trailing-spaces: 0*/
import {PageObjectExample} from './example.po.js';

describe('Example 2', () => {
  let poExample;

  beforeEach(() => {
    poExample = new PageObjectExample();
  });

  it('Displays form information', () => {
    poExample.setFirstName('Joe');
    poExample.setLastName('Smith');
    poExample.setPet('bird');

    // For now there is a timing issue with the binding.
    // Until resolved we will use a short sleep to overcome the issue.
    browser.sleep(200);

    expect(poExample.getMessage()).toEqual('JOE SMITH has a BIRD');
  });
});
