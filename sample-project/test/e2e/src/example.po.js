/*eslint no-trailing-spaces: 0*/
export class PageObjectExample {
  constructor() { }

  setFirstName(value) {
    this._fillTextInput('firstName', value);
  }

  setLastName(value) {
    this._fillTextInput('lastName', value);
  }

  setPet(value) {
    this._selectOption('pet', value);
  }

  getMessage() {
    return element(by.css('.e2e-form-message')).getText();
  }

  _fillTextInput(bindName, value) {
    element(by.valueBind(bindName)).clear().sendKeys(value);
  }

  _selectOption(bindNane, value) {
    element.all(by.optionBind(bindNane)).then((petOptions) => {
      petOptions.forEach((opt) => {
        opt.getText().then(optText => {
          if (optText === value) {
            opt.click();
          }
        });
      });
    });
  }
}
