import moment from 'moment';
import {inject} from 'aurelia-framework';
import {AppConfig} from './app-config';

@inject(AppConfig)
export class DateFormatValueConverter {
  constructor(appConfig) {
    this.appConfig = appConfig;
  }
  toView(value) {
    let result = '';
    if (value) {
      result = moment(value).format(this.appConfig.getDateFormat());
    }
    return result;
  }
}
